// Servico de gerenciamento da escrita de dados em um banco de dados

const util = require('util');
const binance = require("../exchanges/binance");
//const foxbit = require("./exchanges/foxbit")
const coinbase = require("../exchanges/coinbase");
const redis = require('../models/redis'); 

/*
 * Verifica que as APIs estão online
 */
exports.pingApis = () => {
	binance.ping()
		.catch((error) => { throw new Error("Binance not online") });
	
	coinbase.ping()
		.catch((error) => { throw new Error("Coinbase not online") });
}

/*
 * Busca o objeto correto baseado no nome da exchange
 */
getExchange = (exchangeName) => {
	switch(exchangeName) {
		case 'binance':
			exchange = binance;
			break;
		case 'coinbase':
			exchange = coinbase;
			break;
		default:
			exchange = null;
			break
	}
	return exchange;
}

/*
 * Compara precos da mesma moeda entre duas exchanges
 */
getAssetPrice = (asset, exchange) => {
	const ex = getExchange(exchange);
	if (ex === null) {
		throw new Error("Exchange doesn't exist'");
	}

	const price = ex.price(ex.getAssetTicker(asset));
	const result = price.then((apiResponse) => {
		const values = {
			asset: asset,
			exchange: exchange,
			value: ex.getCurrencyPrice(apiResponse),
		}
		return values;
	})
	return result;
}


/*
 * Busca o preco de todos os assets e guarda em um banco de dados
 */
exports.storeAssetPrices = (assets) => {
	// Busca os dados nas APIs
	promiseArray = [];
	for (i in assets) {
	 	promiseArray.push(getAssetPrice(assets[i], 'binance').then((result) => { return result }));	
	 	promiseArray.push(getAssetPrice(assets[i], 'coinbase').then((result) => { return result }));	
	}
	
	// Resolve as promises e reduz os elementos do vetor a objetos enderecados pelo nome do ativo
	Promise.all(promiseArray).then((results) => {
		const priceData = results.reduce((acc, obj) => {
			let key = obj['asset'];
			if (!acc[key]) {
				acc[key] = [];
			}
			delete obj['asset']
			acc[key].push(obj);
			return acc;
		}, {})
		const obj = {
			timestamp: Date.now(),
			data: priceData
		}

		// Salva os dados em um banco de dados não-relacional
		redis.storeData('latest-prices',  JSON.stringify(obj))
	})
}

/*
 * Busca os últimos precos de ativos guardaados no banco de dados
 */
exports.getAssetPrices = () => {
	const data = redis.getData('latest-prices').then((result) => {
		//const out = JSON.parse(result);
		const out = result;
		console.log(util.inspect(out, {showHidden: false, colors: true, depth: null}));
	})
	return data;
}
