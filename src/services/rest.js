// Arquivo que gerencia requests REST

const https = require("https");

exports.get = (host) => {
	return new Promise((resolve, reject) => {
		const req = https.get(host, res => {
			res.on("data", d => {
				resolve(d.toString());
			});
		});
		req.on("error", error => {
			reject(error);
		});
		req.end();
	})
}

exports.post = (host, headers, body) => {
	console.log("todo");
}
