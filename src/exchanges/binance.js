// Arquivo principal das requests relativas à corretora Binance

const rest = require("../services/rest");
const host = "https://api.binance.com/api/v3" 

exports.ping = () => {
	const endpoint = `${host}/time`	
	return rest.get(endpoint)

}

// Busca o preco de um determinado asset na exchange
exports.price = (currency) => {
	const endpoint = `${host}/ticker/price?symbol=${currency}`	
	return rest.get(endpoint)
}

// Pega o campo de preco de acordo com o resultado dessa API
exports.getCurrencyPrice = (apiResponse) => {
	return JSON.parse(apiResponse).price;
}

// Retorna identificador do ativo na exchange
exports.getAssetTicker = (asset) => {
	var ticker = null;
	switch(asset) {
		case 'BTC':
			ticker = 'BTCUSDC';
			break;
		case 'ETH':
			ticker = 'ETHUSDC';
			break;
		case 'XRP':
			ticker = 'XRPUSDC';
			break;
		default:
			ticker = null;
			break;
	}
	return ticker;
}
