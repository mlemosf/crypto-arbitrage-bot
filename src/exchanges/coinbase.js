// Arquivo principal das requests relativas à corretora Coinbase 

const rest = require("../services/rest");
const host = "https://api.coinbase.com/v2"

exports.ping = () => {
	const endpoint = `${host}/time`
	return rest.get(endpoint)
}

// Busca o preco de um determinado asset na exchange
exports.price = (currency) => {
	const endpoint = `${host}/prices/${currency}/spot`	
	return rest.get(endpoint)
}

// Pega o campo de preco de acordo com o resultado dessa API
exports.getCurrencyPrice = (apiResponse) => {
	return JSON.parse(apiResponse).data.amount;
}

// Retorna identificador do ativo na exchange
exports.getAssetTicker = (asset) => {
	var ticker = null;
	switch(asset) {
		case 'BTC':
			ticker = 'BTC-USD';
			break;
		case 'ETH':
			ticker = 'ETH-USD';
			break;
		case 'XRP':
			ticker = 'XRP-USD';
			break;
		default:
			ticker = null;
			break;
	}
	return ticker;
}
