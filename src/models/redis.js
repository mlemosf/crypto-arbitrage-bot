// Interface de acesso ao banco de dados Redis

const redis = require('redis');
const { promisify } = require('util');
const client = redis.createClient();
const getAsync = promisify(client.get).bind(client);

client.on('connect', () => {
	console.log('Database connected');
})

client.on('error', (error) => {
	throw error;
})

client.on('exit', () => {
	console.log('close')
	client.quit();
})

exports.storeData = (objectName, objectData) => {
	client.set(objectName, objectData);
}

exports.getData = (objectName) => {
	return getAsync(objectName)
}
