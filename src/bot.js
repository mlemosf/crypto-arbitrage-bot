// Arquivo principal do TraderBot

const fs = require("fs");
const database = require("./services/database");
const api = JSON.parse(fs.readFileSync("./keys/api.json"))

const binanceCredentials = api[0];
//const foxbitCredentials = api[1]
const coinbaseCredentials = api[2];

main = async () => {
	try {
		assets = ['BTC', 'ETH', 'XRP']
		database.pingApis()
		setInterval(() => { database.storeAssetPrices(assets) }, 5000);
		setInterval(() => { database.getAssetPrices() }, 5000)
	}
	catch (err) {
		console.error(err);
	}
}

main()
