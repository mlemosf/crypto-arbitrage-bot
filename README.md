# Crypto arbitrage bot

## About

Cryptocurrency bot that checks prices against different exchanges to profit on price 
differences.

## Code

### Dependencies

* yarn/npm
* NodeJS
* Redis


### Execution

To execute the project the user has to add it's personal credentials for each of the supported exchanges (Binance, Coinbase). Change the API keys **keys/api.example.json/** to **keys/api.json** and change each value.  
New exchanges can be added as neede but don't have official support (yet).  

To execute the project just run 

	yarn start

on the root of the project.  
The boot will then look for the prices on the exchanges for the requested assets and return a JSON object, that can then be parsed for calculations.


## Contributing

For contributing just start working on an issue and submit a pull request, detailing the feature in detail. Please respect the project's development guidelines.
